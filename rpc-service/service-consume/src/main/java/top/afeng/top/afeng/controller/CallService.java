package top.afeng.top.afeng.controller;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.afeng.IRPCTest;

/**
 * @datetime: 2020/5/25 20:53
 * @project_name: pangu-spring-cloud
 * @author: afeng
 */
@RestController
public class CallService {

    @Reference
    IRPCTest irpcTest;

    @RequestMapping("consumeCallProvider")
    public String serviceMethod(){
        String rpc = irpcTest.Rpc();
        return rpc;
    }
}
