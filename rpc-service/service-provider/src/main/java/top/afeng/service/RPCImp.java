package top.afeng.service;

import lombok.extern.slf4j.Slf4j;
import top.afeng.IRPCTest;

/**
 * @datetime: 2020/5/25 20:50
 * @project_name: pangu-spring-cloud
 * @author: afeng
 */
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class RPCImp implements IRPCTest {
    @Override
    public String Rpc() {
        log.info("调用了dubbo服务");
        return "RPCImp";
    }

}
