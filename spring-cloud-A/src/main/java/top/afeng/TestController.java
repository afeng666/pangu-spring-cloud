package top.afeng;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.afeng.ServiceAFeignClient;

@RestController
public class TestController {

    @Autowired
    ServiceAFeignClient serviceAFeignClient;

    @RequestMapping("/test")
    public String test(){
        return serviceAFeignClient.test();
    }
}