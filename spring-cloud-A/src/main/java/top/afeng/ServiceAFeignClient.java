package top.afeng;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(value = "pangu") //这里的name对应调用服务的spring.applicatoin.name
public interface ServiceAFeignClient {

    @RequestMapping(value = "/test")
    String test();

//    String hi(@RequestParam("id") String id);

}