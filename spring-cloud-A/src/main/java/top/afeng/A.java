package top.afeng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @datetime: 2020/5/23 11:47
 * @project_name: pangu-spring-cloud
 * @author: afeng
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class A {
    public static void main(String[] args) {
        SpringApplication.run(A.class,args);
    }
}
